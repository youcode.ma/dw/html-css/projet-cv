# CV
Créez une simple page web avec HTML/CSS pour présenter votre plus beau CV 

## Périmètre
- Créer une maquette pour le CV   
- Créer la structure HTML de la page CV
- Utiliser CSS pour donner de style à votre CV 

## Critères d'évaluation
- Le rendu contient la maquette du CV (image,screenshot..)
- La page contient au minimum une image de profil et plusieurs paragraphes et titre pour présenter vos informations et expériences
- Tous les div contient un id ou classe
- La struture de la page CV (HTML) est séparé du design/style (CSS) (Aucun attribut style ou balise style dans le document HTML est alloué)
- La qualité du code (suivre le guide de style: https://google.github.io/styleguide/htmlcssguide.html)

## Améliorations possibles 
- Utiliser les balises HTML5 comme header, article, section etc.. pour donner de signification au code HTML
- Adapter le CV pour des écrans de différentes tailles (Desktop, Tablet, Mobile) (Responsive)
- Utiliser le flexbox ou un framework CSS pour le layout de CV
- Mettre le CV enligne en utilisant github pages ( ajouter le lien de votre CV enligne dans le fichier Readme.md) 


